// Существуют другие способы ввода текста (распознавание речи, копировать/вставить с помощью мыши),
// поэтому из <input> не всегда корректно получиться считать данные через события клавиатуры.

document.addEventListener('keyup', (event) =>{
let color = document.querySelector('.color');
if(color){
    color.classList.remove('color');
}
let string = document.querySelector(`[data-name='${event.key}']`);
string.classList.add('color');
console.log(string);
})

